
#city_parishes{
   [zoom > 9]{
      marker-file: url('C:\Users\regerton\Desktop\map_layers10\master/church.png');
      //marker-fill:#007a87;
      marker-line-width: 1;
      marker-allow-overlap:true;
    }
    [zoom = 10]{   
      marker-width:10;
    }
    [zoom = 11]{   
      marker-width:15;
    }
    [zoom = 12]{   
      marker-width:25;
    }
    [zoom = 13]{   
      marker-width:30;
  	
  	::labels {
      text-size: 12;
      text-name: "[parish]";
      text-face-name: "Arial Regular";
      text-fill: #000000;
      //text-wrap-width: 10;
      text-placement-type: simple;
      text-allow-overlap: true;
      text-horizontal-alignment: right;
      text-dx: 12;
      
    // Move overlapping labels
      
      //Belfast
      [canonical = "Belfast city: St. Paul\'s"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Belfast city: Union Workhouse"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Belfast city: Sacred Heart"] { text-horizontal-alignment: left; text-dx: -12; }
      
      //Cork
      [canonical = "Cork city: Blackrock"] { text-horizontal-alignment: middle; text-placements: 'S'; text-dy: 12;}
      [canonical = "Cork city: St. Finbarr\'s (South)"] { text-horizontal-alignment: middle; text-placements: 'S'; text-dy: 12;}
      [canonical = "Cork city: Ss Peter and Paul\'s"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Kilpadder"] { text-horizontal-alignment: middle; text-placements: 'S'; text-dy: 12;}
      [canonical = "Kilshannig"] { text-horizontal-alignment: middle; text-placements: 'S'; text-dy: 12;}
      [canonical = "Glountane"] { text-horizontal-alignment: middle; text-placements: 'S'; text-dy: 12;}
      
      //Dublin
      [canonical = "Blackrock"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Chapelizod"] { text-dy: 1; }
      [canonical = "Dublin city: St. Audoen\'s"] { text-dy: 1; }
      [canonical = "Dublin city: St. Andrew\'s"] { text-horizontal-alignment: middle; text-placements: 'S'; text-dy: 12;}
      [canonical = "Dublin city: St. Nicholas\' (Without)"] { text-horizontal-alignment: middle; text-placements: 'S'; text-dy: 12;}
      [canonical = "Kingstown"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Glasthule"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Ballybrack"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Rathdown Union Workhouse"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Monkstown"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Cabinteely"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Dalkey"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Booterstown"] { text-horizontal-alignment: left; text-dx: -12; }  
      [canonical = "Dublin city: St. James\'"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Dublin city: St. Mary\'s (Pro-Cathedral)"] { text-horizontal-alignment: left; text-dx: -12; text-dy: -6;}    
      [canonical = "Palmerstown"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Fairview"] { text-placements: 'S'; text-dy: 12; text-dx: -20;}
      [canonical = "Dublin city: St. Catherine\'s"] { text-horizontal-alignment: left; text-dx: -10; text-dy: 4;}
      [canonical = "Sandymount"] { text-horizontal-alignment: left; text-dx: -10; text-dy: 4;}
      [canonical = "Greystones"] { text-horizontal-alignment: left; text-dx: -12; }
      
      //Kilkenny
      [canonical = "Kilkenny city: St. Mary\'s"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Kilkenny city: St. Canice\'s"] { text-horizontal-alignment: left; text-dx: -12; }
	  [canonical = "Kilkenny city: St. Patrick\'s"] { text-horizontal-alignment: middle; text-placements: 'S'; text-dy: 12;}
      
      //Limerick
      [canonical = "Limerick city: St. Munchin\'s"] { text-horizontal-alignment: left; text-dx: -12; }
      [canonical = "Limerick city: St. John\'s"] { text-horizontal-alignment: middle; text-placements: 'S'; text-dy: 12;}
      
   	  //Waterford
      [canonical = "Waterford city: St. Patrick\'s and St. Olaf\'s"] { text-horizontal-alignment: middle; text-placements: 'S'; text-dy: 12;}
      [canonical = "Waterford city: Holy Trinity"] { text-horizontal-alignment: left; text-dx: -12; text-dy: -4;}
      
      //Wicklow
      [canonical = "Bray"] { text-horizontal-alignment: middle; text-placements: 'S'; text-dy: 12;}
      [canonical = "Little Bray"] { text-horizontal-alignment: middle; text-placements: 'S'; text-dy: 12;}
    }
  }
}