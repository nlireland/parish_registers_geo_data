

#provinces[zoom = 6]{
  [province = "Ulster"] { polygon-fill: @colour1; }
  [province = "Munster"] { polygon-fill: @colour2; }
  [province = "Leinster"] { polygon-fill: @colour3; }
  [province = "Connacht"] { polygon-fill: @colour4; }
    
  ::outlines{ 
    line-width:0.2; 
  }
  
  ::labels{
    text-name: "[province]";
    text-face-name: "Arial Regular";
    text-fill: #000000;
    text-wrap-width: 10;
    //text-placement-type: simple;
    text-transform: uppercase;
    text-placements: "N,S,E,W,NE,SE,NW,SW";
    
    [province = "Connacht"] { text-dy: -5; } 
  }
}

