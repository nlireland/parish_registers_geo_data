#counties[zoom<9] {
  [county = "Antrim"] { polygon-fill: @colour1; }
  [county = "Armagh"] { polygon-fill: @colour2; }
  [county = 'Carlow'] { polygon-fill: @colour6; }
  [county = "Cavan"] { polygon-fill: @colour2; }
  [county = "Clare"] { polygon-fill: @colour3; }
  [county = "Cork"] { polygon-fill: @colour1; }
  [county = "Derry"] { polygon-fill: @colour4; }
  [county = "Donegal"] { polygon-fill: @colour1; }
  [county = "Dublin"] { polygon-fill: @colour6; }
  [county = "Down"] { polygon-fill: @colour6; }
  [county = "Fermanagh"] { polygon-fill: @colour6; }
  [county = "Galway"] { polygon-fill: @colour1; }
  [county = "Kerry"] { polygon-fill: @colour2; }
  [county = "Kildare"] { polygon-fill: @colour3; }
  [county = "Kilkenny"] { polygon-fill: @colour2; }
  [county = "Laois"] { polygon-fill: @colour1; }
  [county = "Leitrim"] { polygon-fill: @colour3; }
  [county = "Limerick"] { polygon-fill: @colour5; }
  [county = "Longford"] { polygon-fill: @colour6; }
  [county = "Louth"] { polygon-fill: @colour1; }
  [county = "Mayo"] { polygon-fill: @colour6; }
  [county = "Meath"] { polygon-fill: @colour4; }
  [county = "Monaghan"] { polygon-fill: @colour3; }
  [county = "Offaly"] { polygon-fill: @colour2; }
  [county = "Roscommon"] { polygon-fill: @colour5; }
  [county = "Sligo"] { polygon-fill: @colour4; }
  [county = "Tipperary"] { polygon-fill: @colour6; }
  [county = "Tyrone"] { polygon-fill: @colour5; }
  [county = "Waterford"] { polygon-fill: @colour4; }
  [county = "Westmeath"] { polygon-fill: @colour1; }
  [county = "Wexford"] { polygon-fill: @colour3; }
  [county = "Wicklow"] { polygon-fill: @colour5; } 
}

#counties {
   //COMMON
  ::outlines {
    line-color:#007a87;
    line-opacity: 1;
  }
  
  // COUNTY LINES
  [zoom = 10]{
    ::outlines {
      line-width:1;
      line-dasharray: 4, 6;
     }
   }
  [zoom = 11]{
     ::outlines {
       line-width:2;
       line-dasharray: 6, 8;
     }
   }
  [zoom = 12]{
     ::outlines {
      line-width:2;
      line-dasharray: 8, 10;
     }
   }
   [zoom = 13]{
     ::outlines {
      line-width:2;
      line-dasharray: 8, 10;
     }
   }
  
  // LABELLING
  [zoom = 7]{
    ::labels {
      text-size: 9;
    }
  }
  [zoom = 8]{
    ::labels {
      text-size: 16;
    }
  }  
  [zoom = 9]{
    ::labels {
      text-size: 18;
    }
  }    
  [zoom > 6][zoom < 10]{
    ::outlines {
      line-width:1;
    }
    ::labels {
      text-name: "[county]";
      text-face-name: "Arial Regular";
      text-fill: #000000;
      //text-wrap-width: 10;
      //text-placement-type: simple;
      text-transform: uppercase;
    }
    ::labels {
      [county = "Galway"] { text-dx: 2; }
      [county = "Kerry"] { text-dx: 1; }
      [county = "Leitrim"] { text-dy: 20; text-dx: 2; }
      [county = "Offaly"] { text-dy: -5; }
      [county = "Roscommon"] { text-dy: -10; }
    }
  }
} // county end

