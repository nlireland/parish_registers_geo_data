 #dioceses[zoom<9]{ 
  [diocese = "Achonry"] { polygon-fill: @colour1; }
  [diocese = "Ardagh and Clonmacnois"] { polygon-fill: @colour1; }
  [diocese = "Armagh"] { polygon-fill: @colour1; }
  [diocese = "Cashel and Emly"] { polygon-fill: @colour4; }
  [diocese = "Clogher"] { polygon-fill: @colour6; }
  [diocese = "Clonfert"] { polygon-fill: @colour2; }
  [diocese = "Cloyne"] { polygon-fill: @colour6; }
  [diocese = "Cork and Ross"] { polygon-fill: @colour2; }
  [diocese = "Derry"] { polygon-fill: @colour2; }
  [diocese = "Down and Connor"] { polygon-fill: @colour8; }
  [diocese = "Dromore"], { polygon-fill: @colour4; }
  [diocese = "Dublin"] { polygon-fill: @colour6; }
  [diocese = "Elphin"] { polygon-fill: @colour8; }
  [diocese = "Ferns"] { polygon-fill: @colour2; }
  [diocese = "Galway"] { polygon-fill: @colour4; }
  [diocese = "Kerry"] { polygon-fill: @colour1; }
  [diocese = "Kildare and Leighlin"] { polygon-fill: @colour8; }
  [diocese = "Killala"], { polygon-fill: @colour4; }
  [diocese = "Killaloe"] { polygon-fill: @colour7; }
  [diocese = "Kilmore"] { polygon-fill: @colour7; }
  [diocese = "Limerick"] { polygon-fill: @colour8; }
  [diocese = "Meath"], { polygon-fill: @colour4; }
  [diocese = "Ossory"] { polygon-fill: @colour6; }
  [diocese = "Raphoe"], { polygon-fill: @colour4; }
  [diocese = "Tuam"] { polygon-fill: @colour6; }
  [diocese = "Waterford and Lismore"] { polygon-fill: @colour7; }
 }

#dioceses {
  //COMMON
  ::outlines {
    line-color:#007a87;
    line-opacity: 1;
  }
  
  // DIOCESE LINES
  [zoom = 10]{
    ::outlines {
      line-width:1;
     }
   }
  [zoom = 11]{
     ::outlines {
       line-width:2;
     }
   }
  [zoom = 12]{
     ::outlines {
      line-width:2;
     }
   }
   [zoom = 13]{
     ::outlines {
      line-width:2;
     }
   }
  
  // LABELLING
  [zoom = 7]{
    ::labels {
      text-size: 9;
    }
  }
  [zoom = 8]{
    ::labels {
      text-size: 16;
    }
  }  
  [zoom = 9]{
    ::labels {
      text-size: 18;
    }
  }    
  [zoom > 6][zoom < 10]{
    ::outlines {
      line-width:1;
    }
    ::labels {
      text-name: "[diocese]";
      text-face-name: "Arial Regular";
      text-fill: #000000;
      text-wrap-width: 10;
      //text-placement-type: simple;
      text-transform: uppercase;
    }
    ::labels {
      [diocese = "Armagh"] { text-dy: -15; }
      [diocese = "Cloyne"] { text-dy: -10; }
      [diocese = "Galway"] { text-dx: 1; }
      [diocese = "Dublin"] { text-dy: 10; }
      [diocese = "Elphin"] { text-dy: 25; }
      [diocese = "Kildare and Leighlin"] { text-dy: -20; }
      [diocese = "Tuam"] { text-dx: 3; }
      [zoom=9]{
      	[diocese = "Kildare and Leighlin"] { text-dy: -50; }
      }
      
    }
  }
} // county end