

#provinces_ecc[zoom=6]{
  [province = "Armagh"] { polygon-fill: @colour1; }
  [province = "Cashel"] { polygon-fill: @colour2; }
  [province = "Dublin"] { polygon-fill: @colour3; }
  [province = "Tuam"] { polygon-fill: @colour4; }
    
  ::outlines{ 
    line-width:0.2; 
  }
  
  ::labels{
    text-name: "[province]";
    text-face-name: "Arial Regular";
    text-fill: #000000;
    text-wrap-width: 10;
    //text-placement-type: simple;
    text-transform: uppercase;
    text-placements: "N,S,E,W,NE,SE,NW,SW";
    
    [province = "Armagh"] { text-dx: 1; } 
    [province = "Tuam"] { text-dy: -10; } 
    
  }
  
}