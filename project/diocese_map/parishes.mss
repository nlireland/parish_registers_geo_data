#parishes{
  
  [diocese = "Achonry"] { polygon-fill: @colour1; }
  [diocese = "Ardagh and Clonmacnois"] { polygon-fill: @colour1; }
  [diocese = "Armagh"] { polygon-fill: @colour1; }
  [diocese = "Cashel and Emly"] { polygon-fill: @colour4; }
  [diocese = "Clogher"] { polygon-fill: @colour6; }
  [diocese = "Clonfert"] { polygon-fill: @colour2; }
  [diocese = "Cloyne"] { polygon-fill: @colour6; }
  [diocese = "Cork and Ross"] { polygon-fill: @colour2; }
  [diocese = "Derry"] { polygon-fill: @colour2; }
  [diocese = "Down and Connor"] { polygon-fill: @colour8; }
  [diocese = "Dromore"], { polygon-fill: @colour4; }
  [diocese = "Dublin"] { polygon-fill: @colour6; }
  [diocese = "Elphin"] { polygon-fill: @colour8; }
  [diocese = "Ferns"] { polygon-fill: @colour2; }
  [diocese = "Galway"] { polygon-fill: @colour4; }
  [diocese = "Kerry"] { polygon-fill: @colour1; }
  [diocese = "Kildare and Leighlin"] { polygon-fill: @colour8; }
  [diocese = "Killala"], { polygon-fill: @colour4; }
  [diocese = "Killaloe"] { polygon-fill: @colour7; }
  [diocese = "Kilmore"] { polygon-fill: @colour7; }
  [diocese = "Limerick"] { polygon-fill: @colour8; }
  [diocese = "Meath"], { polygon-fill: @colour4; }
  [diocese = "Ossory"] { polygon-fill: @colour6; }
  [diocese = "Raphoe"], { polygon-fill: @colour4; }
  [diocese = "Tuam"] { polygon-fill: @colour6; }
  [diocese = "Waterford and Lismore"] { polygon-fill: @colour7; }
  
  
  //COMMON
  ::outlines {
    line-color:#007a87;
    line-opacity: 1;
  }
  
    // PARISH LINES
  [zoom = 9]{
    ::outlines {
      line-width:0.5;
      line-opacity: 0.3;
     }
   }
  [zoom = 10]{
    ::outlines {
      line-width:1;
     }
   }
  [zoom = 11]{
     ::outlines {
       line-width:2;
     }
   }
  [zoom = 12]{
     ::outlines {
       line-width:3;
     }
   }
   [zoom = 13]{
     ::outlines {
       line-width:2;
     }
   }
  
  // PARISH LABELLING
  [zoom = 10]{
    ::labels {
      text-size: 11;
    }
  }
  [zoom = 11]{
    ::labels {
      text-size: 15;
    }
  }  
  [zoom = 12]{
    ::labels {
      text-size: 20;
    }
  }
  [zoom = 13]{
    ::labels {
      text-size: 15;
    }
  }
  [zoom > 9]{
    ::labels {
      text-name: "[parish]";
      text-face-name: "Arial Regular";
      text-fill: #000000;
      text-wrap-width: 10;
      text-placement-type: simple;
      //text-placements: "N,S,E,W,NE,SE,NW,SW";
      //text-vertical-alignment: middle;
      //text-horizontal-alignment: middle;
        
      // Antrim
        [parish = "Dunloy"] { text-dy: 10; }
        [parish = "Rathlin Island"] { text-dy: -2; }
      
      // Armagh
      	[parish = "Killeavy Upper"] { text-dy: -5; }
        [parish = "Seagoe"] { text-dx: 1; }
      
      // Carlow
        //[parish = "Carlow"] { text-dx: 1; }
      
      // Cavan
        [parish = "Ballintemple"] { text-dy: -3; }
      	[parish = "Corlough"] { text-dx: -1; }
        //[parish = "Drumgoon"] { text-dx: 1; }
      
      // Clare
        [parish = "Feakle"] { text-dy: 15; }
        [parish = "Killkeady"] { text-dy: -10; }
        [parish = "Kilfiddane"] { text-dx: -20; }
        [parish = "O\'Callaghan\'s Mills"] { text-dy: -15;}
      
      // Cork
        [parish = "Aghada"] { text-dy: 5; text-dx: -1;}
        [parish = "Ballyclough"] { text-dy: -2; }
        [parish = "Banteer"] { text-dy: 20; }
        [parish = "Charleville"] { text-dy: 10; }
        [parish = "Carrigaline"] { text-dy: 2; }
        [parish = "Cloyne"] { text-dx: 1; }
        [parish = "Kilnamartyra"] { text-dy: 15; }
      	[parish = "Kinsale"] { text-dy: 5; }
        [parish = "Watergrasshill"] { text-dy: -10; }
      	[parish = "Kilmeen and Castleventry"] { text-dy: -10; }
      
      // Derry
         //St Columbs
      
      // Donegal
        [parish = "Ardara"] { text-dy: 10; }
        [parish = "Umey"] { text-dx: -10; }
      	[parish = "Culdaff"] { text-dx: -10; }
      
      // Dublin
        [parish = "Howth"] { text-dy: 5; text-dx: 10;}
        [parish = "Rolestown"] { text-dy: -10; text-dy: -4; }
      
      // Down
        [parish = "Drumaroad"] { text-dx: 4; }
      
      // Fermanagh
        [parish = "Aughalurcher"] { text-dx: 8; text-dy: -10; }
        [parish = "Innismacsaint"] { text-dx: 10; }
        [parish = "Cleenish"] { text-dy: 4; }
    
      // Galway
        [parish = "Aughrim"] { text-dy: -5; }
        [parish = "Kilkerrin and Clonberne"] { text-dx: 5; }
        [parish = "Oranmore"] { text-dx: 5; }
        [parish = "Peterswell"] { text-dy: 10; } 
        [parish = "Rosmuck"] { text-dy: -10; }
     	[parish = "Omey"] { text-dx: 25; }
      	[parish = "Ardrahan"] { text-dy: -10; } 
      	[parish = "Peterswell"] { text-dy: -10; }
    
      // Kerry
        [parish = "Allihies"] { text-dy: 10; } 
        [parish = "Cahirciveen"] { text-dy: 10; } 
        [parish = "Tralee"] { text-dy: 15; }  
      
      // Kildare
        [parish = "Clonbulloge"] { text-dy: 10; } 
        
      // Kilkenny
        [parish = "Clara"] { text-dy: 10; } 
        [parish = "Kilmacow"] { text-dy: 10; text-dx: 1; } 
        [parish = "Mullinavat"] { text-dy: -10; } 
        [parish = "Windgap"] { text-dx: -4; } 
      
      // Laois
        [parish = "Borris-in-Ossory"] { text-dy: 15; } 
        [parish = "Mountmellick"] { text-dy: 10; } 
      
      // Leitrim
        [parish = "Drumreilly Lower"] { text-dy: 15; text-dx: 5; } 
        [parish = "Kinlough"] { text-dy: -10; } 
    
      // Limerick
        [parish = "Ardagh"] { text-dy: -15; text-dx: 30; } 
        [parish = "Newcastle West"] { text-dy: -12; text-dx: -2; }
        [parish = "Kilcolman"] { text-dy: -5; }
        [parish = "Caherconlish"] { text-dx: 3; }
        [parish = "Lurriga"] { text-dy: -10; }
        [parish = "Pallasgreen and Templebredin"] { text-dy: -5; }
        [parish = "Kilmallock"] { text-dy: 30; }
        [parish = "Bulgaden"] { text-dy: -10; }
      
      // Longford
        [parish = "Clomcille"] { text-dy: 10; }
        
      // Louth
        [parish = "Faughart"] { text-dy: 15; text-dx: -5; }
      
      // Mayo
        [parish = "Achill"] { text-dx: 2; }
        [parish = "Ardagh"] { text-dx: 1; }
        [parish = "Aughagower"] { text-dy: -10; text-dx: 3; }
        [parish = "Bekan"] { text-dy: -10; }
        [parish = "Carracastle"] { text-dy: -5; }
        [parish = "Kilfian"] { text-dy: -10; }
        [parish = "Kilcommon Erris"] { text-dx: 3; }
        [parish = "Kilmore Erris"] { text-dy: -15; }
        //[parish = "Tourmakeady"] { text-dx: -1; }
        
       // Meath
        [parish = "Kells"] { text-dy: -5; }
        //[parish = "Kilcloon, Batterstown and Kilcock"] { text-dy: 1; }
        [parish = "Moybologue"] { text-dx: 1; text-dy: 5; }
        [parish = "Navan"] { text-dx: -3; }
        [parish = "Rathkenny"] { text-dx: -1; }
        [parish = "Summerhill"] { text-dy: 10; }
      
      // Monaghan
        [parish = "Tullycorbet"] { text-dx: -1; }
      
      // Offaly
        [parish = "Tisaron and Galen"] { text-dy: -10; }
      
      // Roscommmon
        [parish = "Strokestown"] { text-dy: 10; }
      
      // Sligo
        [parish = "Achonry"] { text-dx: 1; }
        [parish = "Drumcliff"] { text-dx: 2; }
        [parish = "Ballysodare and Kilvarnet"] { text-dx: 1; }
        [parish = "Easkey"] { text-dy: -15; }
        [parish = "Kilfree and Killaraght"] { text-dy: -4; }
      
     // Tipperary 
        [parish = "Clogheen"] { text-dy: 15; }
        [parish = "Powerstown"] { text-dy: 10; }
        [parish = "Roscrea"] { text-dy: 5; }
      
      // Tyrone
        [parish = "Desertcreight"] { text-dy: 30; }
        [parish = "Donaghedy"] { text-dy: 10; text-dx: 1; }
        [parish = "Killeeshil"] { text-dy: 5; }
        [parish = "Termonmacguirk"] { text-dx: 2; }
        [parish = "Umey"] { text-dx: -1; }
     
     // Waterford
        [parish = "Tramore"] { text-dy: -3; }
      	[parish = "Modeligo"] { text-dy: 10; }
      
     // Westmeath
         [parish = "Mayne"] { text-dx: -1; }
      
     // Wexford
        [parish = "Crossabeg"] { text-dy: 5; }
        [parish = "Gorey"] { text-dy: 5; }
        [parish = "Old Ross"] { text-dx: 1; }
      
     // Wicklow
        [parish = "Arklow"] { text-dy: -5; }
        [parish = "Clonmore"] { text-dy: 5; }
        [parish = "Hacketstown"] { text-dy: -5; }
        [parish = "Tomacork"] { text-dy: 10; text-dx: -1;}
    }
  } // parish labels at zoom 9
  
   [zoom > 10]{
    ::labels {
      text-name: "[parish]";
      text-face-name: "Arial Regular";
      text-fill: #000000;
      text-wrap-width: 10;
      text-placement-type: simple;
  	
      // Limerick
  		[parish = "Ardagh"] { text-dx: 25; } 
  	
      // Galway
     	[parish = "Omey"] { text-dx: 30; } 
    }
  } // parish labels at zoom 10
  
} // parish end
