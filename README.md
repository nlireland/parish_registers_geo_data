

# Overview

This repository contains a group of files used to create and display the map for the [Catholic Parish Registers at the NLI](http://registers.nli.ie/) website.

### License ###
[![licensebuttons by](https://licensebuttons.net/l/by/3.0/88x31.png)](https://creativecommons.org/licenses/by/4.0)

This repository and its data are released under the [Creative Commons CC_BY 4.0 license](https://creativecommons.org/licenses/by/4.0)

### Acknowledgements ###

Please refer to [this text](ACKNOWLEDGEMENTS.md) for acknowledgements.

### Repository Contents ###
+ GeoJSON files (Qgis and Tilemill)
> * city_parishes.geojson
> * counties.geojson
> * dioceses.geojson
> * parishes_ecc.geojson
> * parishes_unmerged.geojson
> * provinces.geojson
> * provinces_ecc.geojson
+ GeoJSON files (Traject Splitter)
> * city_geo_json.json
> * parishes_geo_json.json
+ MBTiles files (Mapbox and Tilestream)
> * city_map.mbtiles
> * county_map.mbtiles
> * diocese_map.mbtiles

### Initial Clone of the Repository ###
+ To make any changes to the map you must first clone the repository:
```
git clone https://bitbucket.org/nlireland/parish_registers_geo_data
```

### Updating the Map ###
+ Any changes to the map will require a cycle of events to take place
> * e.g. Changing the spelling of a label (Follow steps 1-4 below)
> > * Update attribute table of layer in QGIS
> > * New GeoJson layer needed in Tilemill
> > * New GeoJson needed for splitter
> > * New mbtiles files for mapbox and tilestream
> * e.g. Moving a parish line (Follow steps 1-5 below)
> > * Change line in QGIS layer, means new polygon shape/coordinates
> > * New GeoJson layer needed in Tilemill
> > * New GeoJson needed for splitter
> > * New mbtiles files for mapbox and tilestream
> > * New static map image needed for the disambiguation page

#### 1. Updating QGIS ####
[Updating QGIS Shapes](https://bitbucket.org/nlireland/parish_registers_geo_data/wiki/update_QGIS)

#### 2. Updating Tilemill ####
[Updating Tilemill](https://bitbucket.org/nlireland/parish_registers_geo_data/wiki/update_tilemill)

#### 3. GeoJson Splitter ####
[GeoJSON Splitter](https://bitbucket.org/nlireland/parish_registers_geo_data/wiki/update_geojson_for_traject_splitter)

#### 4. Updating Mapbox and Tilestream ####
[Updating Mapbox](https://bitbucket.org/nlireland/parish_registers_geo_data/wiki/update_mapbox_and_tilestream)

#### 5. Generate Static Map Images ####
[Generate Static Map Images](https://bitbucket.org/nlireland/parish_registers_geo_data/wiki/update_static_map_images)

### Creating the Repository ###
+ These steps are in relation to the initial creation of the repositry
+ These steps are not needed as part of the update process and are merely for documentation purposes
+ The steps are as follows:
> 1. [Mapwarper](https://bitbucket.org/nlireland/parish_registers_geo_data/wiki/create_geo-reference_raster_images_mapwarper) - Warp the Irish Times Images
> 1. [QGIS](https://bitbucket.org/nlireland/parish_registers_geo_data/wiki/create_polygon_shapefiles_for_parishes_QGIS) - Create Polygon Shapes
> 1. [Tilemill](https://bitbucket.org/nlireland/parish_registers_geo_data/wiki/create_MBTile_files_for_mapbox_Tilemill) - Style the map images

